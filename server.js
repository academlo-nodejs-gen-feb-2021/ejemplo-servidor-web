const http = require("http"); //Parte del nucleo de node
const fs = require("fs"); //Parte del nucleo de node
const EventEmitter = require("events"); //Parte del nucleo de node
const axios = require("axios");
const morgan = require("morgan"); 

const eventEmmiter = new EventEmitter();

//Creamos el evento -> argumento que recibe eventEmmiter.on() [nombre del evento]
eventEmmiter.on('visita', () => {
    console.log("Se ha vistado la página de contacto");
});

//Definimos en que formato va a imprimir los mensajes en la consola
let logger = morgan('dev'); 

//Primer argumento una función de callback
//Función de callback -> (request, response)
//Request -> Petición o solicitud del cliente
//Response -> Lo que le vamos a dar como respuesta al cliente
http.createServer(async (request, response) => {
    //Se va a ejecutar cada vez que un cliente haga una petición
    const {url, method} = request;

    //Configuración de Morgan para imprimir en consola todas las peticiones que se hagan a nuestro servidor
    logger(request, response, (error) => {
        if(error) return console.log(error.message);
    });


    //Content-Type -> Formato con el cual vamos a responder al cliente
    //MIME Types -> https://developer.mozilla.org/es/docs/Web/HTTP/Basics_of_HTTP/MIME_types
    console.log(method);
    if(method === "GET"){
        switch(url){
            case '/':
                // response.setHeader("Content-Type", "text/html");
                fs.readFile("index.html", (error, data) => {
                    console.log(data);
                    if(error){
                        return response.end("<h1>404</h1>");
                    }
                    return response.end(data);
                    //Finalizamos la respuesta hacía el cliente
                });
                break;
            case '/contacto':
                eventEmmiter.emit('visita'); //Disparamos el evento
                response.setHeader("Content-Type", "text/html; charset=utf-8");
                response.write("<h1>Página de contacto</h1>");
                response.end();
                break;
            case '/pokemons': 
                let pokemones = await obtenerPokemones();
                console.log(pokemones);
                response.end(JSON.stringify(pokemones));
                break;
            default:
                response.statusCode = 404;
                response.setHeader("Content-Type", "text/html; charset=utf-8");
                response.write("<h1>404</h1>")
                response.end();
                break;
        }
    }else if(method === "POST"){
        switch(url){
            case '/registro': 
                //Si la petición es /registro POST -> vamos a guardar los datos que me esté enviando el cliente

                //La acción que ejecutariamos cuando se dispare el evento
                //data -> recibir datos del cliente
                request.on('data', (data) => {
                    //Guardar en la base de datos
                    //Guardar en un archivo, etc...
                    console.log(data.toString());
                });

                //La acción que ejecutariamos cuando se dispare el evento 
                //end -> se termino de recibir los datos del cliente
                request.on('end', () => {
                    //Responder al cliente
                    response.end("<h1>Los datos ya han sido recibidos</h1>");
                });
                break;
            default:
                response.statusCode = 404;
                response.setHeader("Content-Type", "text/html; charset=utf-8");
                response.write("<h1>404</h1>")
                response.end();
                break;
        }
    }
}).listen(8000);

const obtenerPokemones = async() => {
    try{
        //Hacemos una petición con axios
        const results = await axios.get("https://pokeapi.co/api/v2/pokemon/");
        return results.data;
    }catch(error){
        console.log(error);
    }
}